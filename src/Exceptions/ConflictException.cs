﻿namespace Exceptions;

public class ConflictException(string message) : PassInException(message)
{
}