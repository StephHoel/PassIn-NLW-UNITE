﻿namespace Exceptions;

public class PassInException(string message) : SystemException(message)
{
}